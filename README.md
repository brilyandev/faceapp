## HOW TO CLONE
$ git clone https://github.com/AllCareSquad/FaceAppMobile.git

## Description

FaceAppMobile App Repository.

## Installation

```bash
$ npm install
```

## Running the app on Development

```bash
$ npx react-native run-android
```

export interface Participant {
  id: number;
  email: string;
  employee_number: string;
  position: string;
  grade: string;
  health_social_number: string;
  resident_number: string;
  first_name: string;
  last_name: string;
  place_of_birth: string;
  date_of_birth: string;
  citizenship: string;
  identity_type: string;
  identity_number: string;
  gender: string;
  blood_type: string;
  rhesus_type: string;
  height_metric: string;
  weight_metric: string;
  body_weight: string;
  effective_date: Date;
  expired_date: Date;
  is_pension: number;
  status: string;
  reason: string;
  company_id: number;
  progress_id: number;
  remarks: string;
  activity_id: number;
  registration_number: string;
  family_deed_number: string;
  registration_date: Date;
  pension_number: string;
  phone: string;
  category: string;
  photo: string;
  fullname: string;
  pension_type: string;
  group_level: string;
  clinic_provider_id: number;
  name_on_card: string;
  sap_code: string;
  body_height: string;
}

export interface Beneficiary {
  id: number;
  first_name: string;
  last_name: string;
  health_social_number: string;
  registration_number: string;
  height_metric: string;
  weight_metric: string;
  body_weight: string;
  body_height: string;
  place_of_birth: string;
  date_of_birth: string;
  gender: string;
  blood_type: string;
  rhesus_type: string;
  effective_date: string;
  expired_date: string;
  status_id: number;
  participant_id: number;
  is_twin: number;
  photo: string;
  fullname: string;
  resident_number: string;
  domicile?: Domicile;
  phone: string;
  email: string;
}

export interface Domicile {
  id: number;
  address: string;
  rt: string;
  rw: string;
  postal_code: string;
  sub_district_id: string;
  district_id: string;
  region_id: string;
  province_id: string;
  effective_date: Date;
  reason: string;
  remarks: string;
  time_period: string;
}

export interface Profile {
  record_id: number;
  login_id: string;
  email: string;
  reference_id: number;
  is_enable: boolean;
  device_id: string;
  phone_number: string;
  is_participant: boolean;
  participants: Participant;
  kerabat: Beneficiary[];
  domicile?: Domicile;
}

export interface Ecard {
  id: number;
  registration_number: string;
  fullname: string;
  resident_number: null | string;
  kerabat?: Ecard[];
}

export interface BaseResponse<T> {
  requestId: string;
  responseId: string;
  deviceId: string;
  timestamp: string;
  responseCode: number;
  responseStatus: string;
  responseMessage: string;
  data: T;
}

export type ProfileResponse = BaseResponse<Profile>;
export type ECardResponse = BaseResponse<Ecard>;

export interface LoginActivity {
  login_id: string;
  phone_number: string;
  platform: string;
  platform_os: string;
  device_id: string;
  longitude_area: number;
  latitude_area: number;
}

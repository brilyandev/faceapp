module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module:react-native-dotenv',
      {
        moduleName: 'react-native-dotenv',
        path: '.env',
        blacklist: null,
        whitelist: null,
        safe: false,
        allowUndefined: true,
      },
    ],
    [
      'module-resolver',
      {
        cwd: 'babelrc',
        root: ['.'],
        extensions: [
          '.js',
          '.ios.js',
          '.android.js',
          '.ts',
          '.ios.ts',
          '.android.ts',
          '.jsx',
          '.ios.jsx',
          '.android.jsx',
          '.json',
        ],
        alias: {
          Assets: './src/Assets',
          Screens: './src/Screens',
          Services: './src/Services',
          Contexts: './src/Contexts',
          Navigations: './src/Navigations',
          Themes: './src/Themes',
          Config: './src/Config',
          Components: './src/Components',
          Atoms: './src/Components/Atoms',
          Molecules: './src/Components/Molecules',
          Organisms: './src/Components/Organisms',
          Utils: './src/Utils',
        },
      },
    ],
    'react-native-reanimated/plugin',
  ],
};

import StaticVar from 'Config/StaticVar';
import {isURI} from './image-picker';

export function getThumbnail(image: any) {
  return image ? (isURI(image.formats.thumbnail.url) ? image.formats.thumbnail.url : StaticVar.URL_CMS_API + image.formats.thumbnail.url) : 'https://picsum.photos/200';
}

export function getActualImage(image: any) {
  return image ? (isURI(image.url) ? image.url : StaticVar.URL_CMS_API + image.url) : 'https://picsum.photos/600';
}

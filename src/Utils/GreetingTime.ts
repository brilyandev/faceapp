import dayjs from 'dayjs';
function getGreetingTime(m: any) {
  let g = ''; //return g

  m = dayjs(m); //if we can't find a valid or filled moment, we return.

  let split_afternoon = 12; //24hr time to split the afternoon
  let split_evening = 15; //24hr time to split the evening
  let split_night = 18;
  let currentHour = parseFloat(m.format('HH'));

  if (currentHour >= split_afternoon && currentHour < split_evening) {
    g = 'siang';
  } else if (currentHour >= split_evening && currentHour < split_night) {
    g = 'sore';
  } else if (currentHour >= split_night) {
    g = 'malam';
  } else {
    g = 'pagi';
  }

  return g;
}

export default getGreetingTime;

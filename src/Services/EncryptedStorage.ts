import MMKVStorage from 'react-native-mmkv-storage';

const MMKV = new MMKVStorage.Loader().initialize();

class Storage {
  constructor() {
    (async () => {
      // @ts-ignore
      await MMKV.encryption.encrypt();
    })();
  }

  setItem(key: string, data: string) {
    MMKV.setString(key, data);
  }

  getItem(key: string) {
    return MMKV.getString(key);
  }

  removeItem(key: string) {
    MMKV.removeItem(key);
  }

  clear() {
    MMKV.clearStore();
  }
}

const EncryptedStorage = new Storage();

export default EncryptedStorage;

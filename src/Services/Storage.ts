import MMKVStorage from 'react-native-mmkv-storage';

const MMKV = new MMKVStorage.Loader().initialize();

export class Storage {
  static setItem(key: string, data: string) {
    MMKV.setString(key, data);
  }

  static getItem(key: string) {
    return MMKV.getString(key);
  }

  static removeItem(key: string) {
    MMKV.removeItem(key);
  }

  static clear() {
    MMKV.clearStore();
  }
}

export class AsyncStorage {
  static async setItem(key: string, data: string) {
    await MMKV.setStringAsync(key, data);
  }

  static async getItem(key: string) {
    return MMKV.getStringAsync(key);
  }

  static async removeItem(key: string) {
    MMKV.removeItem(key);
  }

  static async clear() {
    MMKV.clearStore();
  }
}

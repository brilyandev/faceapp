/* eslint-disable quotes */
import axios from 'axios';
import {StaticVar} from '../Config';

// ===> api create
const api = axios.create({
  baseURL: StaticVar.URL_CMS_API,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
  // json: true
});

// ===> api list function request

// auth
const getFaq = () => api.get(`/faqs`);
const getBanner = () => api.get(`/banner-managements`, {params: {_sort: 'published_at:desc'}});
const getNewsInformation = () => api.get(`/informasi-terbarus`, {params: {_sort: 'published_at:desc'}});
const getNotifications = (userId: number) => api.get(`/notifications`, {params: {user_id: userId, _sort: 'published_at:desc'}});
const getNotificationCount = (userId: number) => api.get(`/notifications/count`, {params: {user_id: userId, is_read: false}});
const readNotif = ({id, ...data}: {user_id: number; id: number; is_read: boolean}) => api.put(`/notifications/${id}`, data);
const geNewsTag = () => api.get(`/kategoris`, {params: {_sort: 'published_at:desc'}});
const unLikeNews = (id) => api.delete(`/likes/${id}`);
const likeNews = (data) => api.post(`/likes`, data);

export const apis = {
  getFaq,
  getBanner,
  getNewsInformation,
  getNotifications,
  geNewsTag,
  unLikeNews,
  likeNews,
  getNotificationCount,
  readNotif,
};

export default apis;

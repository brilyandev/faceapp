/* eslint-disable quotes */
import axios from 'axios';
import {StaticVar} from '../Config';
import EncryptedStorage from './EncryptedStorage';
import DeviceInfo from 'react-native-device-info';
import {Storage} from './Storage';
import {ECardResponse, LoginActivity, ProfileResponse} from '../../types/interface';

// ===> api create
const api = axios.create({
  baseURL: StaticVar.URL_API,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
  // json: true
});

// ===> api interceptors
api.interceptors.request.use(
  async (config) => {
    let deviceId = await DeviceInfo.getDeviceId();
    let user = EncryptedStorage.getItem('user');
    if (user) {
      let parsedUser: {token: string} = JSON.parse(user);
      if (parsedUser) {
        config.headers.Authorization = `Bearer ${parsedUser.token}`;
      }
      if (deviceId) {
        config.headers['x-device-id'] = deviceId;
      }
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

// ===> api list function request

// auth
const postSignIn = (data) => api.post(`/api/auth`, data);
const postNewOtp = (data) => api.post(`/api/otp`, data);
const postVerifyOtp = (data) => api.post(`/api/otp/verify`, data);
const patchRenewPhonePassword = (data) => api.patch(`/api/user/renew`, data);
const postChangePassword = (data) => api.post(`/api/change-password`, data);
const getValidateToken = () => api.get(`/api/auth/validate-token`);
const checkPin = async () => {
  try {
    let respon = await api.get(`/api/check-pin`);
    let {data} = respon;
    if (data.responseCode === 200) {
      Storage.setItem('pin', data.data.isConfigure ? '1' : '0');
    }
    return data;
  } catch (e) {
    console.log(e.message);
    return {data: {isConfigure: false}};
  }
};
const setPin = (pin) => {
  let body = {
    pin: pin,
  };
  return api.post(`/api/set-pin`, body).then((res) => res.data);
};
const validatePin = async (pin) => {
  let body = {
    pin: pin,
  };
  let respon = await api.post(`/api/validate-pin`, body);
  return respon.data;
};
const postReqByLoginId = (data) => api.post(`/api/otp/request-by-login-id`, data);

//profile
const getUserProfile = () => api.get<ProfileResponse>(`/api/user/participants`);
const patchChangeNumber = (data) => api.patch('/api/user/update-phone', data);
const changeNumberByNoPegawai = (data: {phone_number: string; nomor_pegawai: string}) => api.patch('/api/user/change-phone-by-no-pegawai', data);

// e-cards
const getUserCards = () => api.get<ECardResponse>('/api/user/cards');

const postForgotPassword = (data) => api.post(`/api/forgot-password`, data);

const setFCMTokenDeviceId = (data: {fcm_token_id: string; device_id: string}) => api.post(`/api/auth/fcm-token`, data);
const loginActivity = (data: LoginActivity) => api.post(`/api/auth/activity`, data);

const uploadPhoto = (data: FormData) => api.post(`/upload-photo/user/upload-photo-profile`, data, {headers: {'Content-Type': 'multipart/form-data'}});
const updateProfilePatricipant = (data) => api.patch(`/api/user/update-profile`, data);
const updateProfileBeneficiary = (data) => api.patch(`/api/user/update-profile-bene`, data);
const logout = (token: string) => api.get(`/api/auth/logout`, {headers: {authorization: `Bearer ${token}`}});

export const apis = {
  postSignIn,
  postNewOtp,
  postVerifyOtp,
  postChangePassword,
  patchRenewPhonePassword,
  checkPin,
  setPin,
  validatePin,
  postReqByLoginId,
  getUserProfile,
  getUserCards,
  postForgotPassword,
  getValidateToken,
  setFCMTokenDeviceId,
  patchChangeNumber,
  uploadPhoto,
  changeNumberByNoPegawai,
  updateProfilePatricipant,
  updateProfileBeneficiary,
  logout,
  loginActivity,
};

export default apis;

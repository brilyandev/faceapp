import {API_URL} from 'react-native-dotenv';
import {Edge} from 'react-native-safe-area-context';
//server localhost : ''
// google api key test = AIzaSyDpvpbmz1U4BiEbLHK3DD-pXFJkllkYPhI
// google api key allcare = AIzaSyCJM_YcGyxmYKpe9tkfSzwL81Lr-L-ZbCw

export default {
  URL_API: `${API_URL}`,
  GOOGLE_APIKEY: 'AIzaSyDpvpbmz1U4BiEbLHK3DD-pXFJkllkYPhI',
};

export const defaultEdges: Edge[] = ['top', 'left', 'right'];
export const defaultProfileImage = 'https://storage.googleapis.com/cms-allcare/uploads/user_a2df794571/user_a2df794571.png';

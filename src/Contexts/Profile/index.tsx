import React, {useContext, createContext, useState} from 'react';
import dayjs from 'dayjs';
import {useApiPrivate} from 'Services/index';
import {Beneficiary, Profile} from '../../../types/interface';

export type StatusEnum = 'loading' | 'success' | 'error' | 'switch' | 'idle';

export interface ProfileData {
  participantId: number | null;
  beneficiaryId: number | null;
  fullname: string;
  firstName: string;
  lastName: string;
  /** @description flag for active account (switch account) */
  isPrimary: boolean;
  /** @description no pegawai */
  loginId: string | null;
  employeeNumber: string | null;
  bloodType: string;
  age?: number;
  bodyHeight: string;
  unitHeight: string;
  bodyWeight: string;
  unitWeight: string;
  email: string;
  phoneNumber: string | null;
  placeOfBirth: string;
  dateOfBirth: string;
  address?: string;
  rt?: string;
  rw?: string;
  region?: string;
  district?: string;
  subDistrict?: string;
  isCurrentLogin: boolean;
  gender: string;
  rhesusType: string;
  registrationNumber: string;
  postalCode?: string;
  province?: string;
  alternativePhone: string | null;
  photo: string;
}

type ProfileState = {
  status: StatusEnum;
  data: ProfileData[];
  activeIndex: number;
};

type ProfileAction =
  | {type: typeof REQUEST; status: StatusEnum}
  | {type: typeof SUCCESS; data: ProfileData[]; status: StatusEnum; activeIndex: number}
  | {type: typeof ERROR; status: StatusEnum}
  | {type: typeof SET_PRIMARY; index: number}
  | {type: typeof SET_PHOTO; index: number; file: string}
  | {type: typeof IDLE};

const REQUEST = 'PROFILE_REQUEST';
const SUCCESS = 'PROFILE_SUCCESS';
const ERROR = 'PROFILE_ERROR';
const SET_PRIMARY = 'SET_PRIMARY_PROFILE';
const SET_PHOTO = 'SET_PHOTO_PROFILE';
const IDLE = 'IDLE_PROFILE';

function profileReducer(state: ProfileState, action: ProfileAction): ProfileState {
  switch (action.type) {
    case REQUEST: {
      return {...state, status: 'loading'};
    }
    case SUCCESS: {
      return {...state, status: 'success', data: [...action.data]};
    }
    case ERROR: {
      return {...state, status: 'error'};
    }
    case SET_PRIMARY: {
      let data = state.data.map((item, index) => {
        return {...item, isPrimary: index === action.index};
      });
      return {...state, status: 'switch', data, activeIndex: action.index};
    }
    case SET_PHOTO: {
      let data = state.data.map((item, index) => {
        return {...item, photo: index === action.index ? action.file : item.photo};
      });
      return {...state, status: 'success', data};
    }

    default: {
      return {...state, status: 'idle'};
    }
  }
}

type ParticipantType = Omit<Profile, 'kerabat'>;
type BeneficiaryType = Beneficiary;

function ProfileAdapter(data: ParticipantType, loginAsParticipant: boolean): ProfileData {
  return {
    participantId: data.participants.id,
    beneficiaryId: null,
    fullname: data.participants.fullname,
    firstName: data.participants.first_name,
    lastName: data.participants.last_name,
    isPrimary: loginAsParticipant,
    loginId: loginAsParticipant ? data.login_id : null,
    employeeNumber: data.participants.employee_number,
    bloodType: data.participants.blood_type,
    // TODO: calculate with dayjs
    age: data.participants.date_of_birth ? dayjs().diff(dayjs(data.participants.date_of_birth), 'year') : undefined,
    bodyHeight: data.participants.body_height,
    unitHeight: data.participants.height_metric,
    bodyWeight: data.participants.body_weight,
    unitWeight: data.participants.weight_metric,
    email: loginAsParticipant ? data.email : data.participants.email,
    phoneNumber: loginAsParticipant ? data.phone_number : null,
    placeOfBirth: data.participants.place_of_birth,
    dateOfBirth: data.participants.date_of_birth,
    address: data.domicile?.address,
    rt: data.domicile?.rt,
    rw: data.domicile?.rw,
    region: data.domicile?.region_id,
    district: data.domicile?.district_id,
    subDistrict: data.domicile?.sub_district_id,
    isCurrentLogin: data.is_participant,
    gender: data.participants.gender,
    rhesusType: data.participants.rhesus_type,
    registrationNumber: data.participants.registration_number,
    postalCode: data.domicile?.postal_code,
    province: data.domicile?.province_id,
    alternativePhone: data.participants.phone,
    photo: data.participants.photo,
  };
}

function ProfileBeneficiaryAdapter(data: BeneficiaryType, identity: Profile, loginAsParticipant: boolean): ProfileData {
  return {
    participantId: null,
    beneficiaryId: data.id,
    fullname: data.fullname,
    firstName: data.first_name,
    lastName: data.last_name,
    isPrimary: !loginAsParticipant,
    loginId: null,
    employeeNumber: null,
    bloodType: data.blood_type,
    // TODO: calculate with dayjs
    age: data.date_of_birth ? dayjs().diff(dayjs(data.date_of_birth), 'year') : undefined,
    bodyHeight: data.body_height,
    unitHeight: data.height_metric,
    bodyWeight: data.body_weight,
    unitWeight: data.weight_metric,
    email: !loginAsParticipant ? identity.email : data.email,
    phoneNumber: !loginAsParticipant ? identity.phone_number : data.phone,
    placeOfBirth: data.place_of_birth,
    dateOfBirth: data.date_of_birth,
    address: data.domicile?.address,
    rt: data.domicile?.rt,
    rw: data.domicile?.rw,
    region: data.domicile?.region_id,
    district: data.domicile?.district_id,
    subDistrict: data.domicile?.sub_district_id,
    isCurrentLogin: !loginAsParticipant,
    gender: data.gender,
    rhesusType: data.rhesus_type,
    registrationNumber: data.registration_number,
    postalCode: data.domicile?.postal_code,
    province: data.domicile?.province_id,
    alternativePhone: data.phone,
    photo: data.photo,
  };
}

interface Ctx {
  profileAction: {
    getProfile: (idx?: number) => Promise<[ProfileData[], number]>;
    switchAccount: (index: number) => void;
    setPhoto: (index: number, file) => void;
  };
  profileState: ProfileState;
  dataProfile: [];
  setDataProfile: any[];
  loading: boolean;
  setLoading: React.Dispatch<React.SetStateAction<true | Boolean>>;
}

const ProfileContext = createContext<Ctx>({} as Ctx);

function ProfileProvider(props) {
  const [profileState, setProfileData] = React.useReducer(profileReducer, {status: 'idle', data: [], activeIndex: 0});
  const [dataProfile, setDataProfile] = useState<Array<any>>([]);
  const [loading, setLoading] = useState<Boolean | true>(true);

  const profileAction = React.useMemo(
    () => ({
      getProfile: async (idx?: number) => {
        try {
          setProfileData({type: REQUEST, status: 'loading'});
          let response = await useApiPrivate.getUserProfile();
          if (response.status === 200) {
            if (response.data.data) {
              const {data} = response.data;
              let activeIndex = idx || 0;
              const profiles = [ProfileAdapter(data, data.is_participant)];
              for (let index = 0; index < data.kerabat.length; index++) {
                const bene = data.kerabat[index];
                const dataProfileBene = ProfileBeneficiaryAdapter(bene, data, data.is_participant);
                activeIndex = dataProfileBene.isPrimary ? index : activeIndex;
                profiles.push(dataProfileBene);
              }
              setProfileData({type: SUCCESS, data: profiles, status: 'success', activeIndex});
              return [profiles, activeIndex];
            }
          }
          setProfileData({type: IDLE});
        } catch (e) {
          setProfileData({type: ERROR, status: 'error'});
          console.log(e.response?.data);
          console.log(e.message);
        }
        return [[], 0];
      },
      switchAccount: (index: number) => setProfileData({type: SET_PRIMARY, index}),
      setPhoto: (index: number, file: string) => setProfileData({type: SET_PHOTO, index, file}),
    }),
    [],
  );

  return <ProfileContext.Provider value={{profileAction, profileState, dataProfile, setDataProfile, loading, setLoading}} {...props} />;
}

const ProfileContexts = () => useContext(ProfileContext);

export const IndexProfileContext = {
  ProfileProvider,
  ProfileContexts,
};

export default IndexProfileContext;

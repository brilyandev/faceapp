import React, {createContext, useContext, useCallback, useEffect} from 'react';
import {useApiPrivate} from 'Services/';
import {AxiosError} from 'axios';
import EncryptedStorage from 'Services/EncryptedStorage';
import SplashScreen from 'react-native-splash-screen';
import useProfile from '../Profile';

interface IAuthState {
  userId: number | null;
  isSignout: boolean;
  userToken: string | null;
  phoneNumber: string | null;
}

interface IAuth {
  signIn: () => any;
  signOut: () => any;
  createOtp: () => any;
  verifyOtp: () => any;
  changePassword: () => any;
  authContext: {[key: string]: any};
  authState: IAuthState;
}

const AuthContext = createContext<IAuth>({} as IAuth);

function AuthProvider(props) {
  const {profileAction} = useProfile.ProfileContexts();
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userId: action.userId,
            userToken: action.token,
            isLoading: false,
            phoneNumber: action.phoneNumber,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            userId: action.userId,
            isSignout: false,
            userToken: action.token,
            phoneNumber: action.phoneNumber,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
            userId: null,
            phoneNumber: null,
          };
        case 'UPDATE_PHONE': {
          return {
            ...prevState,
            phoneNumber: action.phoneNumber,
          };
        }
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
      phoneNumber: null,
    },
  );

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      try {
        let result = await useApiPrivate.getValidateToken();
        if (result.data.data.valid) {
          let user = EncryptedStorage.getItem('user');
          if (user) {
            let parsedUser = JSON.parse(user);
            if (parsedUser) {
              let [profiles, activeIndex] = await profileAction.getProfile();
              let identity = profiles[activeIndex];
              dispatch({type: 'RESTORE_TOKEN', token: parsedUser.token, phoneNumber: parsedUser.phoneNumber || identity?.phoneNumber, userId: parsedUser.userId});
            }
          }
        }
      } catch (e) {
        // Restoring token failed
        authContext.signOut();
      } finally {
        SplashScreen.hide();
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token

        dispatch({type: 'SIGN_IN', token: data.token, phoneNumber: data.phoneNumber});
      },
      signOut: () => dispatch({type: 'SIGN_OUT'}),
      signUp: async (data) => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token

        dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
      },
      updatePhone: (phoneNumber: string) => dispatch({type: 'UPDATE_PHONE', phoneNumber}),
    }),
    [],
  );

  const signIn = useCallback(async (values) => {
    try {
      const result = await useApiPrivate.postSignIn(values);
      EncryptedStorage.setItem('user', JSON.stringify(result.data.data));
      authContext.signIn(result.data.data);
      return {status: 'OK', result};
    } catch (error) {
      const err = error as AxiosError;
      return {status: 'Failed', error};
    }
  }, []);

  const signOut = async () => {
    useApiPrivate.logout(state.userToken);
    authContext.signOut();
    EncryptedStorage.removeItem('user');
    return 'OK';
  };

  const createOtp = async (data) => {
    try {
      if (data) {
        const sendData = {phoneNumber: `${data}`};
        const result = await useApiPrivate.postNewOtp(sendData);
        return {status: 'OK', result};
      }
      return {status: 'Failed', data: 'Phone Undefined'};
    } catch (error) {
      const err = error as AxiosError;
      return {status: 'Failed', error};
    }
  };

  const verifyOtp = async (data) => {
    try {
      // const sendData = {otp: data};
      const result = await useApiPrivate.postVerifyOtp(data);
      return {status: 'OK', result};
    } catch (error) {
      const err = error as AxiosError;
      return {status: 'Failed', error};
    }
  };

  const changePassword = async (data) => {
    try {
      const result = await useApiPrivate.postChangePassword(data);
      return {status: 'OK', result};
    } catch (error) {
      const err = error as AxiosError;
      return {status: 'Failed', error};
    }
  };

  return <AuthContext.Provider value={{signIn, signOut, createOtp, verifyOtp, changePassword, authContext, authState: state}} {...props} />;
}

const AuthContexts = () => useContext(AuthContext);

export const IndexAuthContexts = {
  AuthProvider,
  AuthContexts,
};

export default IndexAuthContexts;

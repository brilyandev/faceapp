import React, { useContext, createContext, useState } from "react";
import { Alert, PermissionsAndroid } from "react-native";
import {CMS_API_URL} from 'react-native-dotenv';
import Geolocation from 'react-native-geolocation-service';

const LocationContext = createContext({} as any);

function LocationProvider(props) {

  const [currentDirection, setCurrentDirection] = useState<any>({latitude:"",longitude:""});
  const [myLocation, setMyLocation] = useState<any>({latitude:"",longitude:""});
  const [loading, setLoading] = useState<Boolean | false>();
  
  const getCurrentLocation = async () => { 
    Geolocation.getCurrentPosition(
      position => {
        console.log('Location Founded',position.coords.latitude, position.coords.longitude);
        setCurrentDirection({...currentDirection,latitude : position.coords.latitude , longitude : position.coords.longitude})
        setMyLocation({...myLocation,latitude : position.coords.latitude , longitude : position.coords.longitude})
      },
      error => {
        Alert.alert(
          'Oopss!',
          error.message,
          [
            {
              text: 'OK',
              onPress: () => console.log('Cancel Pressed'),
            },
          ],
        );
        console.log(error.message);
      },
      {
        accuracy: {
          android: 'high',
          ios: 'best',
        },
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
        distanceFilter: 0,
        forceRequestLocation: true,
        showLocationDialog: true,
      },
    );
  }
  
  return (
    <LocationContext.Provider value={{ currentDirection,myLocation,setMyLocation,getCurrentLocation,setCurrentDirection,loading, setLoading }} {...props}/>
  );
}

const LocationContexts = () => useContext(LocationContext);

export const IndexLocationContext = {
  LocationProvider, LocationContexts
}

export default IndexLocationContext
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomeScreen, SettingScreen} from 'Screens/index';
import {Text} from 'react-native';
import Icon from './Fragments/Icon';
import {Primary} from 'Themes/Colors';
import Images from 'Themes/Images';
import {useStyleSheet} from '@ui-kitten/components';
import themedStyles from './Styles';
import {useAuth} from 'Contexts/index';

const Tab = createBottomTabNavigator();

function TabNavigator() {
  const styles = useStyleSheet(themedStyles);
  const {authState} = useAuth.AuthContexts();

  return (
    <Tab.Navigator screenOptions={{tabBarActiveTintColor: Primary, tabBarStyle: styles.containerTabs, headerShown: false}}>
      <Tab.Screen
        name="Home"
        options={{
          tabBarIcon: (evaProps) => <Icon name="home" {...evaProps} />,
          tabBarLabel: (evaProps) => (
            <Text style={styles.textTabsFooter} {...evaProps}>
              Beranda
            </Text>
          ),
        }}
        component={HomeScreen}
      />
      <Tab.Screen
        name="Setting"
        options={{
          tabBarLabel: (evaProps) => (
            <Text style={styles.textTabsFooter} {...evaProps}>
              Setting
            </Text>
          ),
          tabBarIcon: (evaProps) => <Icon name="bell-outline" {...evaProps} />
        }}
        component={SettingScreen}
      />
    </Tab.Navigator>
  );
}

export default React.memo(TabNavigator);

import * as React from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Alert, BackHandler} from 'react-native';
import {useAuth} from 'Contexts/index';

export default function useAuthNavigator() {
  const navigation = useNavigation();
  const {authState} = useAuth.AuthContexts();
  const routes = useRoute();
  const [valRoutes, setValRoutes] = React.useState('');

  const handleBackAuth = React.useCallback(
    () =>
      navigation.addListener('beforeRemove', async (e) => {
        e.preventDefault();
        if (routes.name === 'Home') {
          Alert.alert('Konfirmasi', 'Yakin Keluar Aplikasi?', [
            {
              text: 'Batal',
              onPress: () => null,
              style: 'cancel',
            },
            {text: 'Ya', onPress: () => BackHandler.exitApp()},
          ]);
        }
      }),
    [navigation, routes.name],
  );

  return {
    done: 'OK',
    valRoutes,
    checkFirstSignin: authState.phoneNumber,
    handleBackAuth,
  };
}

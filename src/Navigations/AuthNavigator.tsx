import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {
  HomeScreen
} from 'Screens/index';

const Stack = createStackNavigator();

function AuthStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={HomeScreen} />
    </Stack.Navigator>
  );
}

export default React.memo(AuthStack);

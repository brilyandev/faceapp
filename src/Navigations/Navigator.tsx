import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TabNavigator from './TabNavigator';
import AppNavigator from './AppNavigator';
import {useAuth} from '../Contexts';

const Stack = createStackNavigator();

const Navigator: React.FC<{}> = () => {
  const {authState} = useAuth.AuthContexts();

  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {/* {firstTime ? (
        <Stack.Screen name="Welcome" component={WelcomeScreen} />
      ) : authState.userToken ? ( */}
        <>
          <Stack.Screen name="Tab" component={TabNavigator} />
          <Stack.Screen name="App" component={AppNavigator} />
        </>
      {/* ) : (
        <Stack.Screen name="Auth" component={AuthNavigator} />
      )} */}
    </Stack.Navigator>
  );
};

export default React.memo(Navigator);

import React from 'react';
import {InitialState, NavigationContainer, NavigationContainerRef, NavigationState, PartialState, Route} from '@react-navigation/native';
import Navigator from './Navigator';
import {AppState} from 'react-native';
import {loadDeviceInfo, locationPermission} from '../PermissionRequest';
import Analytics from 'appcenter-analytics';

type MaybeActiveRoute = (Omit<Route<string>, 'key'> & {key?: string; state?: InitialState}) | undefined;
// Gets the current screen from navigation state
export const getActiveRoute = (state: NavigationState | PartialState<NavigationState> | undefined): MaybeActiveRoute => {
  // @ts-ignore
  const route: MaybeActiveRoute = state?.routes[state?.index];

  if (route?.state) {
    // Dive into nested navigators
    return getActiveRoute(route.state);
  }

  return route;
};
export const getActiveRouteName = (state: NavigationState | undefined) => getActiveRoute(state)?.name;
export const getActiveRouteParams = (state: NavigationState | undefined) => getActiveRoute(state)?.params;
export default function Router() {
  const ref = React.useRef<NavigationContainerRef<{}>>(null);
  const routeNameRef = React.useRef<string | undefined>('');
  const routeParamRef = React.useRef<{[key: string]: any} | undefined>({});
  const appState = React.useRef(AppState.currentState);
  React.useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  const _handleAppStateChange = (nextAppState) => {
    if (appState.current.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
      locationPermission();
      loadDeviceInfo();
    }

    appState.current = nextAppState;
    console.log('AppState', appState.current);
  };

  React.useEffect(() => {
    console.log('ref.current', ref.current);
    let navState = ref.current?.getRootState();
    routeNameRef.current = getActiveRouteName(navState);
    routeParamRef.current = getActiveRouteParams(navState);
    console.log('routeNameRef.current vv', routeNameRef.current);
    console.log('routeParamRef.current vv', routeParamRef.current);
  }, []);

  return (
    <NavigationContainer
      ref={ref}
      onStateChange={(navState) => {
        const previousRouteName = routeNameRef.current;
        // const previousRouteParams = routeParamRef.current;
        const currentRouteName = getActiveRouteName(navState);
        const currentRouteParams = getActiveRouteParams(navState);
        if (previousRouteName !== currentRouteName) {
          console.log('prevRoute', previousRouteName);
          console.log('currentRouteName', currentRouteName);
          if (!__DEV__) {
            Analytics.trackEvent('Navigation Change', {screen: currentRouteName ?? '', params: currentRouteParams ? JSON.stringify(currentRouteParams) : ''});
          }
        }
        routeNameRef.current = currentRouteName;
        routeParamRef.current = currentRouteParams;
      }}>
      <Navigator />
    </NavigationContainer>
  );
}

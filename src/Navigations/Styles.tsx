import {StyleService} from '@ui-kitten/components';
import {White, LightGrayFooter, GrayTitle} from 'Themes/Colors';
import Type from 'Themes/Typography';

const themedStyles = StyleService.create({
  root: {
    flex: 1,
    flexDirection: 'column',
  },
  containerTabs: {
    height: Type.size60,
    paddingTop: Type.size10,
    paddingBottom: Type.size10,
  },
  textTab: {
    fontSize: Type.size12,
    fontFamily: 'Gilroy-Medium',
    color: LightGrayFooter,
  },

  badge: {
    marginTop: -10,
    backgroundColor: '#F05945',
    fontFamily: 'Gilroy-Bold',
    borderRadius: 5,
    fontSize: Type.size12,
    color: White,
  },

  qrCodeBtn: {
    width: Type.size55,
    height: Type.size55,
    borderRadius: Type.size55,
    justifyContent: 'center',
    backgroundColor: '#53C1A8',
    marginBottom: 5,
    // zIndex: 4000,
  },
  qrCodeIcon: {
    width: Type.size20,
    height: Type.size20,
    alignSelf: 'center',
  },
  textTabsFooter: {
    fontSize: Type.size12,
    fontFamily: 'Gilroy-Medium',
    color: LightGrayFooter,
  },
  icon: {
    width: 25,
    height: 25,
  },
  titleTxt: {
    color: GrayTitle,
    fontWeight: '600',
    fontSize: Type.size20,
    marginLeft: -10,
  },
  titleTxt2: {
    color: White,
    fontWeight: '600',
    fontSize: Type.size20,
    marginLeft: -10,
  },
});

export default themedStyles;

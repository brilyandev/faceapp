import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {
  HomeScreen,
} from 'Screens/index';

const Stack = createStackNavigator();

function AppStack() {
  return (
    <Stack.Navigator initialRouteName="Profile" screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={HomeScreen} />
    </Stack.Navigator>
  );
}

export default React.memo(AppStack);

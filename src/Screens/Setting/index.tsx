import React, {useEffect} from 'react';
import {NavigationParams, NavigationScreenProp} from 'react-navigation';
import {StatusBar} from 'react-native';
import {useStyleSheet,Text} from '@ui-kitten/components';
import themedStyles from './Styles';
import {RouteProp} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {defaultEdges} from 'Config/index';

export interface Props {
  navigation: NavigationScreenProp<any, NavigationParams>;
  route: RouteProp<{}, never>;
}

const Index: React.FC<Props> = ({navigation}) => {
  const styles = useStyleSheet(themedStyles);

  return (
    <SafeAreaView style={styles.root} edges={defaultEdges}>
      <StatusBar backgroundColor="#47D6B6" translucent barStyle="light-content" />
      <Text>Setting</Text>
    </SafeAreaView>
  );
};

export default Index;

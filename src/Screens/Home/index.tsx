/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {NavigationParams, NavigationScreenProp} from 'react-navigation';
import {ImageBackground, StatusBar} from 'react-native';
import {Text, useStyleSheet} from '@ui-kitten/components';
import themedStyles from './Styles';
import {RouteProp, useFocusEffect} from '@react-navigation/native';
import UseAuthNavigator from 'Navigations/Hooks/UseAuthNavigator';
import Images from 'Themes/Images';
import {SafeAreaView} from 'react-native-safe-area-context';
import {defaultEdges} from 'Config/index';
import useCamera from './Hooks/UseCamera';

export interface Props {
  navigation: NavigationScreenProp<any, NavigationParams>;
  route: RouteProp<{}, never>;
}

const Index: React.FC<Props> = ({navigation}) => {
  const styles = useStyleSheet(themedStyles);
  const {handleBackAuth} = UseAuthNavigator();
  const { handleOpenCamera } = useCamera()

  useEffect(() => {
    const sub = handleBackAuth();
    return sub;
  }, [handleBackAuth]);

  useEffect(()=>{
    handleOpenCamera()
  },[])

  return (
    <SafeAreaView style={styles.root} edges={defaultEdges}>
      <StatusBar backgroundColor="#47D6B6" translucent barStyle="light-content" />
      <Text>Home</Text>
    </SafeAreaView>
  );
};

export default Index;

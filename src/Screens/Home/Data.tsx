import {scale} from 'react-native-size-matters';
import Images from 'Themes/Images';

const size = scale(25);

const menuListData = [
  {
    title: 'Faskesku',
    type: 'Tab',
    navigate: 'HomeScreen',
    icon: Images.faskeskuIcon,
    comingSoon: true,
    size,
  },
  {
    title: 'FAQ',
    type: 'App',
    navigate: 'Faq',
    icon: Images.faqIcon,
    comingSoon: false,
    size,
  },

  {
    title: 'Contact\nCenter',
    type: 'App',
    navigate: 'ContactCenter',
    icon: Images.contactCenterIcon,
    comingSoon: false,
    size,
  },
  {
    title: 'Syarat & \nKetentuan',
    type: 'App',
    navigate: 'TermsAndConditions',
    icon: Images.tandcIcon,
    comingSoon: false,
    size: scale(35),
  },
];

const bannerData = [
  {
    title: '',
    content: '',
    image: Images.bannerImg,
  },
  {
    title: '',
    content: '',
    image: Images.bannerImg,
  },
];

const tagData = [
  {
    name: 'Vaksin',
    image: '',
  },
  {
    name: 'COVID-19',
    image: '',
  },
  {
    name: 'Pandemi',
    image: '',
  },
  {
    name: 'Sinnovac',
    image: '',
  },
];

export {menuListData, bannerData, tagData};

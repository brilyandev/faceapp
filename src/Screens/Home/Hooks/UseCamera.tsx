import React, {useCallback} from 'react';
import {PermissionsAndroid, Platform} from 'react-native';
import {useProfile} from 'Contexts/index';
import * as ImagePicker from 'Utils/image-picker';
import apis from 'Services/ApiPrivate';
import ImageResizer from 'react-native-image-resizer';
import {ProfileData} from 'Contexts/Profile';

export default function UseCamera() {

  const handleOpenCamera = useCallback(async () => {
    try {
      if (Platform.OS === 'android') {
        const isGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
        if (!isGranted) {
          const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
            title: 'AllCare Butuh Akses ke Kamera',
            message: 'Izinkan AllCare untuk akses kamera Anda ' + 'agar bisa mengambil gambar',
            buttonNeutral: 'Tanyakan Nanti',
            buttonNegative: 'Batal',
            buttonPositive: 'Setuju',
          });
          if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
            console.log('Camera permission denied');
            return;
          }
        }
      }
      // launch the camera with the following settings
      const image = await ImagePicker.launchCameraAsync({
        mediaType: 'photo',
        quality: 1,
        // includeBase64: true,
      });

      if (!image.didCancel) {
        const source = image.assets[0];
        console.log("soure",source)
      }
    } catch (error) {
      console.log(error.message);
    } finally {

    }
  }, []);

  return {
    handleOpenCamera
  };
}

import 'react-native-gesture-handler';
import * as React from 'react';
import codePush from 'react-native-code-push';
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {Router} from './Navigations';
import {customTheme} from 'Themes/Theme';
import FlashMessage from 'react-native-flash-message';
import {useAuth, useLocation} from './Contexts';
import {MultiProvider, NotificationInit} from './Config';
import RNLocation from 'react-native-location';
import {Storage} from 'Services/Storage';
import {checkLocationPermission, loadDeviceInfo, locationPermission} from './PermissionRequest';

// uncomment below to test from welcome screen again
// Storage.removeItem('installed');

NotificationInit();

let App: React.FC<{}> = () => {
  let [granted, setGranted] = React.useState(false);
  React.useEffect(() => {
    (async () => {
      setGranted(await checkLocationPermission());
    })();
    loadDeviceInfo();
    locationPermission();
  }, []);
  React.useEffect(() => {
    if (granted) {
      let unsubscribe = RNLocation.subscribeToLocationUpdates((locations) => {
        if (locations.length) {
          Storage.setItem('currentDirection', JSON.stringify(locations[0]));
        }
      });
      return unsubscribe;
    }
  }, [granted]);
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={{...eva.light, ...customTheme}}>
        <MultiProvider
          providers={[
            <useAuth.AuthProvider />,
            <useLocation.LocationProvider />,
          ]}>
          <Router />
          <FlashMessage position={'bottom'} />
        </MultiProvider>
      </ApplicationProvider>
    </>
  );
};

let codePushOptions = {checkFrequency: __DEV__ ? codePush.CheckFrequency.MANUAL : codePush.CheckFrequency.ON_APP_RESUME};
App = codePush(codePushOptions)(App);
App.displayName = 'App';
export default App;

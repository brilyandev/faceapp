import React from 'react';
import {View, TouchableOpacity, Text, StyleProp, ViewStyle} from 'react-native';
import {useStyleSheet, Icon} from '@ui-kitten/components';
import themedStyles from './Styles';
import {useNavigation} from '@react-navigation/native';
import {Black} from 'Themes/Colors';

export interface Props {
  title: string | any;
  titleScreen: boolean;
  size: string;
  style: StyleProp<ViewStyle>;
  onBackPress?: () => void;
}

const HeaderApps: React.FC<Props> = (props) => {
  const styles = useStyleSheet(themedStyles);
  const navigation = useNavigation();
  const {titleScreen, title, style, size} = props;

  return (
    <View style={[styles.containerHeader, style ? style : null]}>
      <View style={styles.containerBack}>
        <TouchableOpacity onPress={props.onBackPress ? props.onBackPress : () => navigation.goBack()}>
          <Icon name={'arrow-ios-back-outline'} style={styles.iconBack} fill={Black} />
        </TouchableOpacity>
        {titleScreen ? size == 'large' ? <Text style={styles.title}>{title}</Text> : <Text style={styles.smallTitle}>{title}</Text> : null}
      </View>
    </View>
  );
};

export default HeaderApps;

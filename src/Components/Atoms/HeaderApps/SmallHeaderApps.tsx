import React from 'react';
import { View, TouchableOpacity, Text, StyleProp, ViewStyle } from 'react-native';
import { useStyleSheet, Icon } from '@ui-kitten/components';
import themedStyles from './Styles';
import { useNavigation } from '@react-navigation/native';
import { Primary, White, Black } from "Themes/Colors"

export interface Props {
    title: string | any;
    titleScreen: boolean;
    style: StyleProp<ViewStyle>;
}

const HeaderApps: React.FC<Props> = (props) => {
    const styles = useStyleSheet(themedStyles)
    const navigation = useNavigation();
    const { titleScreen, title, style } = props

    return (
        <View style={[styles.containerHeader, style]}>
            <View style={styles.containerBack}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon name={"arrow-ios-back-outline"} style={styles.iconBack} fill={Black} />
                </TouchableOpacity>
                {titleScreen ? (
                    <Text style={styles.title}>{title}</Text>
                ): null}
            </View>
        </View>
    );
};

export default HeaderApps;

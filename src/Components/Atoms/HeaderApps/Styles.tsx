import {StyleService} from '@ui-kitten/components';
import Metrics from 'Themes/Metrics';
import Type from 'Themes/Typography';
import {White, GrayTitle} from 'Themes/Colors';

const themedStyles = StyleService.create({
  containerHeader: {
    display: 'flex',
    backgroundColor: White,
    paddingHorizontal: Type.size30,
    paddingBottom: Metrics.screenHeight / 90,
  },

  containerBack: {
    display: 'flex',
  },
  iconBack: {
    marginLeft: Metrics.screenWidth / -37.5,
    width: Type.size30,
    height: Type.size30,
    color: GrayTitle,
  },

  title: {
    color: GrayTitle,
    fontSize: Type.size25,
    fontFamily: 'Gilroy-Bold',
    paddingVertical: Type.size7,
  },

  smallTitle: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: Type.size15,
    textAlign: 'center',
    color: '#000',
    paddingHorizontal: Metrics.screenWidth / 10,
  },
});

export default themedStyles;

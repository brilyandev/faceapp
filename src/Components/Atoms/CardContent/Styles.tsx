import { StyleSheet, Platform, StatusBar } from 'react-native';
import { StyleService } from '@ui-kitten/components';
import Metrics from "Themes/Metrics"
import { Primary, White } from "Themes/Colors"

const themedStyles = StyleService.create({
    containerCard : {
        backgroundColor:White,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1, 
        borderRadius:15
    }
});

export default themedStyles;

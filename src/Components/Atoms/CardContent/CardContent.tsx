import React from 'react';
import { View, StatusBar, ViewStyle, ColorValue } from 'react-native';
import { useStyleSheet } from '@ui-kitten/components';
import themedStyles from './Styles';
import { Primary, White } from "Themes/Colors"

export interface Props {
    width : number | string | any,
    height: number | string | any,
    content:any
}

const CardContent: React.FC<Props> = (props) => {
    const styles = useStyleSheet(themedStyles)
    const { width , height, content } = props

    return (
        <View style={[styles.containerCard,{width , height}]}>
            {content}
        </View>
    );
};

export default CardContent;

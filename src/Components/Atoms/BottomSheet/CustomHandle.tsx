import React, {useMemo} from 'react';
import {StyleProp, StyleSheet, View, ViewStyle} from 'react-native';
import {BottomSheetHandleProps} from '@gorhom/bottom-sheet';
import Animated, {Extrapolate, interpolate, useAnimatedStyle} from 'react-native-reanimated';
import Metrics from 'Themes/Metrics';
import {LightGrayFooter} from 'Themes/Colors';

interface HandleProps extends BottomSheetHandleProps {
  style?: StyleProp<ViewStyle>;
}

const Handle: React.FC<HandleProps> = ({style, animatedIndex}) => {
  //#region styles
  const containerStyle = useMemo(() => [styles.header, style], [style]);
  const containerAnimatedStyle = useAnimatedStyle(() => {
    const borderTopRadius = interpolate(animatedIndex.value, [1, 2], [20, 0], Extrapolate.CLAMP);
    return {
      borderTopLeftRadius: borderTopRadius,
      borderTopRightRadius: borderTopRadius,
    };
  });
  //#endregion

  // render
  return (
    <Animated.View style={[containerStyle, containerAnimatedStyle]} renderToHardwareTextureAndroid={true}>
      <View style={styles.indicator} />
    </Animated.View>
  );
};

export default Handle;

const styles = StyleSheet.create({
  header: {
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 14,
    borderBottomWidth: 1,
    borderBottomColor: 'transparent',
  },
  indicator: {
    position: 'absolute',
    marginTop: Metrics.screenHeight / 150,
    marginBottom: Metrics.screenHeight / 150,
    width: Metrics.screenWidth / 8,
    height: 4,
    backgroundColor: LightGrayFooter,
    borderRadius: 50,
  },
});

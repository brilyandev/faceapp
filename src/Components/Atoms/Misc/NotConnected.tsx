import React from 'react';
import {StyleProp, ImageStyle, Image, Text, View, StyleSheet} from 'react-native';
import {scale, verticalScale} from 'react-native-size-matters';
import Metrics from 'Themes/Metrics';
import Images from 'Themes/Images';

export default function NotConnected() {
  return (
    <View style={styles.emptyFaqContainer}>
      <Image source={Images.dataNotFound} style={styles.messageImage as StyleProp<ImageStyle>} />
      <Text>Maaf data tidak ditemukan</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  emptyFaqContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginVertical: verticalScale(Metrics.screenHeight / 10),
  },
  messageImage: {
    width: scale(157),
    height: scale(187),
    resizeMode: 'contain',
  },
});

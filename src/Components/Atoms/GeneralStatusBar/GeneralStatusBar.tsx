import React from 'react';
import {View, StatusBar, ViewStyle, ColorValue, StyleProp} from 'react-native';
import {useStyleSheet} from '@ui-kitten/components';
import themedStyles from './Styles';
import {Primary, White} from 'Themes/Colors';

export interface Props {
  backgroundStatusBar: ViewStyle | any;
  barStyle: any;
  style?: StyleProp<ViewStyle>;
}

const GeneralStatusBar: React.FC<Props> = (props) => {
  const styles = useStyleSheet(themedStyles);
  const {backgroundStatusBar, barStyle} = props;

  return (
    <View style={[styles.statusBar, props.style]}>
      <StatusBar translucent backgroundColor={backgroundStatusBar} {...props} barStyle={barStyle} />
    </View>
  );
};

export default GeneralStatusBar;

import {Platform, StatusBar} from 'react-native';
import {StyleService} from '@ui-kitten/components';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const themedStyles = StyleService.create({
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
});

export default themedStyles;

import React, {useEffect, useRef} from 'react';
import MapView, {Marker} from 'react-native-maps';
import {StyleProp, ViewStyle, TouchableOpacity} from 'react-native';

import {Layout, useStyleSheet, Icon} from '@ui-kitten/components';
import themedStyles from './Styles';
import Images from 'Themes/Images';
import Metrics from 'Themes/Metrics';
import {useLocation} from 'Contexts/index';
import {Black} from 'Themes/Colors';
import {scale} from 'react-native-size-matters';

export interface Props {
  style: StyleProp<ViewStyle>;
  onLocationChange: any;
}

const GoogleMapView: React.FC<Props> = (props) => {
  const styles = useStyleSheet(themedStyles);
  const mapRef = useRef<any>(null);
  const {getCurrentLocation, currentDirection, setCurrentDirection} = useLocation.LocationContexts();
  const markerRef = useRef<any>(null);

  useEffect(() => {
    getCurrentLocation();
  }, []);

  const onPressZoomIn = () => {
    setCurrentDirection({
      latitude: currentDirection.latitude,
      longitude: currentDirection.longitude,
      latitudeDelta: currentDirection.latitudeDelta * 10,
      longitudeDelta: currentDirection.longitudeDelta * 10,
    });
    animateToRegion();
  };

  const onPressZoomOut = () => {
    setCurrentDirection({
      latitude: currentDirection.latitude,
      longitude: currentDirection.longitude,
      latitudeDelta: currentDirection.latitudeDelta * 10,
      longitudeDelta: currentDirection.longitudeDelta * 10,
    });
    animateToRegion();
  };

  const animateToRegion = () => {
    mapRef.current.animateToRegion(currentDirection, 100);
  };

  const onRegionChange = (region) => {
    markerRef.current.animateMarkerToCoordinate(region);
    props.onLocationChange(region)
  }

  return (
    <Layout style={[styles.container, props.style]}>
      <MapView
        ref={mapRef}
        onRegionChange={onRegionChange}
        showsMyLocationButton={true}
        style={{
          flex: 1,
          width: Metrics.screenWidth / 1,
          minHeight: scale(500),
        }}
        region={{
          latitude: currentDirection ? Number(currentDirection.latitude) : 0,
          longitude: currentDirection ? Number(currentDirection.longitude) : 0,
          latitudeDelta: 0,
          longitudeDelta: 0,
        }}
        zoomEnabled={true}>
        <Marker
          ref={markerRef}
          image={Images.markerLocationIcon}
          coordinate={{latitude: currentDirection ? Number(currentDirection.latitude) : 0, longitude: currentDirection ? Number(currentDirection.longitude) : 0}}
        />
        <TouchableOpacity style={styles.zoomIconBtn} onPress={onPressZoomIn}>
          <Icon name={'plus-outline'} style={styles.iconZoomSize} fill={Black} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.zoomIconBtn} onPress={onPressZoomOut}>
          <Icon name={'minus-outline'} style={styles.iconZoomSize} fill={Black} />
        </TouchableOpacity>
      </MapView>
    </Layout>
  );
};

export default GoogleMapView;

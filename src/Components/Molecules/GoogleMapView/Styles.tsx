import {StyleService} from '@ui-kitten/components';
import Metrics from "Themes/Metrics"

const themedStyles = StyleService.create({
    container: {
      flex:1,
      width:"100%",
      backgroundColor: '#fff',
      flexDirection: 'row',
      height:100,
      marginHorizontal:5,
      borderRadius:10,
      justifyContent: 'flex-start',
      alignItems: 'stretch',
    },
    iconAccount: {
      marginTop:7,
      marginLeft:10,
      width: 25,
      height: 25,
    },
    zoomIconBtn:{
      justifyContent:"center",
      display:"flex",
      borderRadius:20
    },
    iconZoomSize: {
      width: 25,
      height: 25,
    },
  });

export default themedStyles;

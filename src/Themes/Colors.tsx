export const Primary = '#2794EB';
export const PrimaryDark = '#2794EB';
export const PrimaryLight = '#6B7176';
export const BlueLight = '#B6DEFF';
export const Secondary = '#767676';
export const White = '#FFFFFF';
export const Black = '#000000';
export const BlackMethalic = '#1B1D28';
export const RedHeart = '#F05945';
export const Steel = '#CCCCCC';
export const Green = '#27AE60';
export const Transparent = 'transparent';
export const BorderColor = '#BDC3D6';

// LINEAR GRADIENT
export const LinierGradient = ['#16B5AC', '#283FAD', '#0F1C73'];

// ACTIONS
export const Success = '#3adb76';
export const Warning = '#ffae00';
export const Alert = '#cc4b37';

// GRAYSCALE
export const GrayLight = '#e6e6e6';
export const GrayMedium = '#cacaca';
export const GrayDark = '#8a8a8a';
export const GrayLight2 = '#fff';

export const GrayTitle = '#212529';
export const GrayText = '#6C757D';
export const GrayTextNotification = '#343A40';

export const LightGray = '#f8f8f8';
export const LightGrayFooter = '#CED4DA';


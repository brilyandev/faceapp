import { Dimensions, StatusBar, Platform } from "react-native"

const isAndroid = Platform.OS === 'android'
const { width: screenWidth, height: screenHeight } = Dimensions.get("window")

const is_X_Ratio = ((screenHeight / screenWidth) == (812 / 375))

const guidelineBaseWidth = 375
const guidelineBaseHeight = is_X_Ratio ? 812 : 667


/**
 * Screen Width
 */
const sWidth = screenWidth

/**
 * Screen Height
 */
const currentHeight = StatusBar?.currentHeight as number
const sHeight = isAndroid ? screenHeight - currentHeight : screenHeight

/**
 * Horizontal Size Scale
 * @param {number} size 
 */
const hScale = (size: number) => (sWidth / guidelineBaseWidth) * size

/**
 * Vertical Size Scale
 * @param {number} size 
 */
const vScale = (size: number) => (sHeight / guidelineBaseHeight) * size


/**
 * Font Size Scale
 * @param {number} size 
 * @param {number} [factor] 
 */
const fScale = (size: number, factor: number = 0.5) => size + (hScale(size) - size) * factor

const rcScale = (width: number, height: number) => ({
    width: hScale(width),
    height: vScale(height),
})

/**
 * Scaled Square
 * @example 
 * someStyle: {
 *  ...sqScale(20),
 *  backgroundColor: 'cyan'
 * }
 * @param size 
 */
const sqScale = (size: number) => ({
    alignItems: "center",
    justifyContent: "center",
    height: hScale(size),
    width: hScale(size),
})

/**
 * Scaled Circle
 * @example 
 * someStyle: {
 *  ...crScale(20),
 *  backgroundColor: 'cyan'
 * }
 * @param size 
 */
const crScale = (size: number) => ({
    alignItems: "center",
    justifyContent: "center",
    height: hScale(size),
    width: hScale(size),
    borderRadius: hScale(size) / 2
})


/**
 * Total Size Scale
 * @param {number} size 
 */
const tScale = (size: number) => (Math.sqrt((sHeight * sHeight) + (sWidth * sWidth)) * size) / 100

export { rcScale, sqScale, crScale, tScale,sHeight, sWidth, hScale, vScale, fScale }
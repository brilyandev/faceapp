// export images
const Images = {
  //IMAGES
  logo: require('../Assets/Images/big-logo.png'),
  logoAllCare: require('../Assets/Images/logo.png'),
  welcomeImg1: require('../Assets/Images/img_welcome1.png'),
  welcomeImg2: require('../Assets/Images/img_welcome2.png'),
  welcomeImg3: require('../Assets/Images/img_welcome3.png'),
  profileImg: require('../Assets/Images/img-profile.png'),
  starChecklistImg: require('../Assets/Images/success-check.png'),
  bannerImg: require('../Assets/Images/banner.png'),
  cardHomeImg: require('../Assets/Images/img-card-home.png'),
  eCardImg: require('../Assets/Images/e-card.png'),
  qrCodeImg: require('../Assets/Images/qr-code-example.png'),
  photoProfile: require('../Assets/Images/photo-profile.png'),
  imgDetailInformation: require('../Assets/Images/ImageDetailInformation.png'),
  imgInformation: require('../Assets/Images/img-information.png'),
  mapImg: require('../Assets/Images/mapImg.png'),
  logoHomeImg: require('../Assets/Images/logo-home.png'),
  fasilitasImg: require('../Assets/Images/fasilitasimg.png'),
  fotoDokter: require('../Assets/Images/fotodokter.png'),
  detailJadwalBack: require('../Assets/Images/detailjadwalback.png'),
  dataNotFound: require('../Assets/Images/data-not-found.png'),
  noInternet: require('../Assets/Images/internet-not-found.png'),
  bg: require('../Assets/Images/background.png'),

  //ICON
  fingerprintIcon: require('../Assets/Icons/fingerprint-icon.png'),
  facebookIcon: require('../Assets/Icons/facebook-icon.png'),
  whatsappIcon: require('../Assets/Icons/whatsapp-icon.png'),
  twitterIcon: require('../Assets/Icons/twitter-icon.png'),
  sehatkuIcon: require('../Assets/Icons/sehatku.png'),
  faskeskuIcon: require('../Assets/Icons/faskesku.png'),
  tandcIcon: require('../Assets/Icons/t&c-icon.png'),
  instagramIcon: require('../Assets/Icons/instagram-icon.png'),
  fasilitasKesehatanIcon: require('../Assets/Icons/fasilitas-kesehatan.png'),
  pindahFakesIcon: require('../Assets/Icons/pindah-fakes.png'),
  pindahFakesIcon2: require('../Assets/Icons/pindah-fakes-icon.png'),
  faqIcon: require('../Assets/Icons/faq.png'),
  contactCenterIcon: require('../Assets/Icons/contact-center.png'),
  qrCodeIcon: require('../Assets/Icons/qr-code.png'),
  contactWhiteIcon: require('../Assets/Icons/contact-white.png'),
  whatsappWhiteIcon: require('../Assets/Icons/whatsapp-white-icon.png'),
  notificationMail: require('../Assets/Icons/notification-message.png'),
  notificationBirthday: require('../Assets/Icons/notification-birthday.png'),
  camera: require('../Assets/Icons/camera.png'),
  calenderProfile: require('../Assets/Icons/calender-profile.png'),
  bloodProfile: require('../Assets/Icons/blood-profile.png'),
  heightProfile: require('../Assets/Icons/height-profile.png'),
  weightProfile: require('../Assets/Icons/weight-profile.png'),
  profilFaskesIcon: require('../Assets/Icons/profil-faskes-icon.png'),
  dokterIcon: require('../Assets/Icons/dokter-icon.png'),
  rsPertaminaImg: require('../Assets/Images/rspertamina.png'),
  jadwalIcon: require('../Assets/Icons/jadwalicon.png'),
  mapIcon: require('../Assets/Icons/mapicon.png'),
  circleIcon: require('../Assets/Icons/circleIcon.png'),
  logoHome: require('../Assets/Icons/logo-home.png'),
  minOutlineIcon: require('../Assets/Icons/min-outline.png'),
  plusOutlineIcon: require('../Assets/Icons/plus-outline.png'),
  markerLocationIcon: require('../Assets/Icons/marker-location.png'),

  editIcon: require('../Assets/Icons/edit.png'),
  lockIcon: require('../Assets/Icons/lock.png'),
  lockUlangiIcon: require('../Assets/Icons/lock-ulangi-password.png'),
  callIcon: require('../Assets/Icons/call.png'),
  loginIcon: require('../Assets/Icons/login.png'),
  emailIcon: require('../Assets/Icons/email.png'),
  phoneIcon: require('../Assets/Icons/phone.png'),
  addressIcon: require('../Assets/Icons/address.png'),
  calendarIcon: require('../Assets/Icons/Calendar.png'),
  locationIcon: require('../Assets/Icons/Location.png'),
  // pindahFaskesIcon2: require('../Assets/Icons/pindah-faskes-icon.png'),
  cameraOutline: require('../Assets/Icons/camera-outline.png'),
  gallery: require('../Assets/Icons/gallery.png'),
  pesan: require('../Assets/Icons/Message.png'),
  artikel: require('../Assets/Icons/Document.png'),
  aktivitas: require('../Assets/Icons/activity.png'),
  ultah: require('../Assets/Icons/ultah.png'),
};

export default Images;

import {PermissionsAndroid, Platform} from 'react-native';
import RNLocation from 'react-native-location';
import DeviceInfo from 'react-native-device-info';
import {Storage} from 'Services/Storage';

export const checkLocationPermission = () => {
  return RNLocation.checkPermission({
    ios: 'whenInUse', // or 'always'
    android: {
      detail: 'fine', // or 'fine'
    },
  });
};

export const locationPermission = async () => {
  RNLocation.configure({
    distanceFilter: 5.0,
  });
  let granted = await checkLocationPermission();
  if (!granted) {
    granted = await RNLocation.requestPermission({
      ios: 'whenInUse',
      android: {
        detail: 'fine',
        rationale: {
          title: 'FaceApp Butuh Akses Lokasi',
          message: 'Izinkan FaceApp untuk akses lokasi Anda',
          buttonNegative: 'Batal',
          buttonPositive: 'Setuju',
        },
      },
    });
  }
  if (granted) {
    let location = await RNLocation.getLatestLocation();
    if (location) {
      Storage.setItem('currentDirection', JSON.stringify(location));
    }
    // RNLocation.subscribeToLocationUpdates((locations) => {
    // console.log(locations[0]);
    // });
  }

  return granted;
};

export const loadDeviceInfo = async () => {
  let deviceName = await DeviceInfo.getDeviceName();
  let systemName = DeviceInfo.getSystemName();
  let systemVersion = DeviceInfo.getSystemVersion();
  let deviceId = await DeviceInfo.getDeviceId();
  let carrier = await DeviceInfo.getCarrier();
  console.log(deviceName, systemName, systemVersion, deviceId, carrier);
  let deviceInfo = {
    deviceName,
    systemName,
    systemVersion,
    deviceId,
    carrier,
  };
  Storage.setItem('deviceInfo', JSON.stringify(deviceInfo));
};
